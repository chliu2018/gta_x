from ase.io import *
from ase.calculators.vasp import VaspChargeDensity as VCD
from ase.units import Bohr

name = 'Cu_111'
vasp_dens = VCD('LOCPOT')
vol = vasp_dens.atoms[0].get_volume()
write('isosurf_vr_%s_desc_c2v.cube'%name, images = vasp_dens.atoms[0],data = vasp_dens.chg[0]*vol)
