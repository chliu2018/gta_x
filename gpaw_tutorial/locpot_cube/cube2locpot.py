from ase.units import Bohr
from ase.io.cube import read_cube_data
from ase.calculators.vasp import VaspChargeDensity as VCD

name = 'Cu_111'
data, atoms = read_cube_data('isosurf_vr_%s_desc.cube'%name)
vol = atoms.get_volume()
vasp_dens = VCD(filename = None)
vasp_dens.atoms.append(atoms)
vasp_dens.chg.append(data/vol)
vasp_dens.write(filename = 'LOCPOT',format = 'chgcar')
