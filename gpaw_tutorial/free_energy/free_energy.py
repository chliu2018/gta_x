import numpy as np
from ase.vibrations import Vibrations
from ase.io import *
from ase import *
from ase.thermochemistry import HarmonicThermo

name = 'co'
surf = 'cu100'
task = 'vibration'

vib_list = [16,17]#atom indices for adsorbates

potentialenergy = np.loadtxt('Energy_%s_%s_%s.txt' % (name,surf,task))
slab = read('geo_ini.traj')

f = open('Summary_%s_%s_thermochem.txt'%(name,surf),'w')
f.write('Helmholtz free energy:\n')
vib = Vibrations(slab,vib_list,'vib_%s_%s_%s'%(name,surf,task),0.01,4)
vib.run()

vib_energies = vib.get_energies()
vib_energies = vib_energies[np.array([i == np.real(i) for i in vib_energies])]

thermo = HarmonicThermo(vib_energies=vib_energies,potentialenergy=potentialenergy)
G = thermo.get_helmholtz_energy(temperature=298.15)
f.write('%.3 eV\n'%G)
f.close()
