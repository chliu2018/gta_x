from gpaw import *
from ase.vibrations import Vibrations
from ase.io import *
import numpy as np

name = 'co'
surf = 'cu100'
task = 'vibration'
vib_list = [16,17]#atom indices for adsorbates

kpts = {'density':2.5,'gamma':True}
xc = 'RPBE'

slab = read('geo_ini.traj')#should be optimized geometry!
slab.pbc = (1,1,0)

calc = GPAW(h = 0.2, #grid space 0.2 Å
            kpts = kpts,
            xc = xc,
            symmetry = {'point_group':False}, #space symmetry
            txt = "%s_%s_%s_gpaw.txt"%(name,surf,task),
            mixer = Mixer(0.1, 5, weight=100.0),
            occupations=FermiDirac(0.1)
)

slab.set_calculator(calc)
e_f = slab.get_potential_energy()

f = open('Energy_%s_%s_%s.txt' % (name,surf,task), 'w')
f.write('%.6f\n' % e_f)
f.close()

vib = Vibrations(slab,vib_list,'vib_%s_%s_%s'%(name,surf,task),0.01,4)
vib.run()
vib.summary(log='vib_%s_%s_%s.log'%(name,surf,task))
vib.write_mode()
vib_energies = vib.get_energies()
