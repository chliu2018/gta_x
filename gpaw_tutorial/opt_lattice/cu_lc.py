from ase import Atoms
from ase.build import bulk
from ase.optimize.bfgs import BFGS
from ase.constraints import UnitCellFilter
from ase.io import Trajectory
from gpaw import GPAW
from gpaw import PW
import numpy as np

f = open('lattice_constant_cu.txt','w')
a = bulk('Cu','fcc')
calc = GPAW(xc='RPBE',
            mode=PW(500),
            # mode=PW(400, cell=a.get_cell()),  # fix no of planewaves!
            kpts={'density':3.5,'gamma':True},
            convergence={'eigenstates': 1.e-10},  # converge tightly!
            txt='cu_lattice_gpaw.txt')
a.set_calculator(calc)

uf = UnitCellFilter(a)
relax = BFGS(uf,logfile = 'cu_lc.log')
traj = Trajectory('cu_lc.traj', 'w', a)
relax.attach(traj)
relax.run(fmax=0.00001)#can be higher if not converging

b = a.get_cell()[0][1]*2
f.write('Relaxed lattice parameter: a = %s A' % b)
f.close()
