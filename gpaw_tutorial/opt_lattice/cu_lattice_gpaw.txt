
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.4.0
 |___|_|             

User:   chliu@qcl18n06
Date:   Tue Jun 25 10:44:17 2019
Arch:   x86_64
Pid:    30332
Python: 2.7.15
gpaw:   /opt/gpaw/1.4.0/lib/python/gpaw
_gpaw:  /opt/gpaw/1.4.0/lib/python/_gpaw.so
ase:    /opt/ase/ase (version 3.16.0-e240cec52a)
numpy:  /opt/intel/intelpython2/lib/python2.7/site-packages/numpy (version 1.14.0)
scipy:  /opt/intel/intelpython2/lib/python2.7/site-packages/scipy (version 1.0.0)
units:  Angstrom and eV
cores:  4

Input parameters:
  convergence: {eigenstates: 1e-10}
  kpts: {density: 3.5,
         gamma: True}
  mode: {ecut: 500.0,
         name: pw}
  xc: RPBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

Cu-setup:
  name: Copper
  id: 573fbe99048a1be4abd65b9829f98c51
  Z: 29
  valence: 11
  core: 18
  charge: 0.0
  file: /usr/local/lib/python/gpaw-setups-0.9.20000/Cu.RPBE.gz
  cutoffs: 1.06(comp), 2.06(filt), 2.43(core), lmax=2
  valence states:
                energy  radius
    4s(1.00)    -4.495   1.164
    4p(0.00)    -0.718   1.164
    3d(10.00)    -4.953   1.058
    *s          22.716   1.164
    *p          26.494   1.164
    *d          22.259   1.058

  Using partial waves for Cu as LCAO basis

Reference energy: -45027.129361

Spin-paired calculation

Occupation numbers:
  Fermi-Dirac: width=0.1000 eV

Convergence criteria:
  Maximum total energy change: 0.0005 eV / electron
  Maximum integral of absolute density change: 0.0001 electrons
  Maximum integral of absolute eigenstate change: 1e-10 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 48

  ( 1  0  0)  ( 1  0  0)  ( 1  0  0)  ( 1  0  0)  ( 1  0 -1)  ( 1  0 -1)
  ( 1  0 -1)  ( 1 -1  0)  ( 0  1  0)  ( 0  0  1)  ( 1  0  0)  ( 1 -1  0)
  ( 1 -1  0)  ( 1  0 -1)  ( 0  0  1)  ( 0  1  0)  ( 1 -1  0)  ( 1  0  0)

  ( 1  0 -1)  ( 1  0 -1)  ( 1 -1  0)  ( 1 -1  0)  ( 1 -1  0)  ( 1 -1  0)
  ( 0  1 -1)  ( 0  0 -1)  ( 1  0  0)  ( 1  0 -1)  ( 0 -1  1)  ( 0 -1  0)
  ( 0  0 -1)  ( 0  1 -1)  ( 1  0 -1)  ( 1  0  0)  ( 0 -1  0)  ( 0 -1  1)

  ( 0  1  0)  ( 0  1  0)  ( 0  1  0)  ( 0  1  0)  ( 0  1 -1)  ( 0  1 -1)
  ( 1  0  0)  ( 0  1 -1)  ( 0  0  1)  (-1  1  0)  ( 1  0 -1)  ( 0  1  0)
  ( 0  0  1)  (-1  1  0)  ( 1  0  0)  ( 0  1 -1)  ( 0  0 -1)  (-1  1  0)

  ( 0  1 -1)  ( 0  1 -1)  ( 0  0  1)  ( 0  0  1)  ( 0  0  1)  ( 0  0  1)
  ( 0  0 -1)  (-1  1  0)  ( 1  0  0)  ( 0  1  0)  ( 0 -1  1)  (-1  0  1)
  ( 1  0 -1)  ( 0  1  0)  ( 0  1  0)  ( 1  0  0)  (-1  0  1)  ( 0 -1  1)

  ( 0  0 -1)  ( 0  0 -1)  ( 0  0 -1)  ( 0  0 -1)  ( 0 -1  1)  ( 0 -1  1)
  ( 1  0 -1)  ( 0  1 -1)  ( 0 -1  0)  (-1  0  0)  ( 1 -1  0)  ( 0  0  1)
  ( 0  1 -1)  ( 1  0 -1)  (-1  0  0)  ( 0 -1  0)  ( 0 -1  0)  (-1  0  1)

  ( 0 -1  1)  ( 0 -1  1)  ( 0 -1  0)  ( 0 -1  0)  ( 0 -1  0)  ( 0 -1  0)
  ( 0 -1  0)  (-1  0  1)  ( 1 -1  0)  ( 0  0 -1)  ( 0 -1  1)  (-1  0  0)
  ( 1 -1  0)  ( 0  0  1)  ( 0 -1  1)  (-1  0  0)  ( 1 -1  0)  ( 0  0 -1)

  (-1  1  0)  (-1  1  0)  (-1  1  0)  (-1  1  0)  (-1  0  1)  (-1  0  1)
  ( 0  1  0)  ( 0  1 -1)  (-1  0  1)  (-1  0  0)  ( 0  0  1)  ( 0 -1  1)
  ( 0  1 -1)  ( 0  1  0)  (-1  0  0)  (-1  0  1)  ( 0 -1  1)  ( 0  0  1)

  (-1  0  1)  (-1  0  1)  (-1  0  0)  (-1  0  0)  (-1  0  0)  (-1  0  0)
  (-1  1  0)  (-1  0  0)  ( 0  0 -1)  ( 0 -1  0)  (-1  1  0)  (-1  0  1)
  (-1  0  0)  (-1  1  0)  ( 0 -1  0)  ( 0  0 -1)  (-1  0  1)  (-1  1  0)

1331 k-points: 11 x 11 x 11 Monkhorst-Pack grid
56 k-points in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1331
   1:     0.09090909    0.09090909    0.00000000          6/1331
   2:     0.09090909    0.09090909    0.09090909          8/1331
   3:     0.18181818    0.09090909    0.09090909         12/1331
   4:     0.18181818    0.18181818    0.00000000          6/1331
   5:     0.18181818    0.18181818    0.09090909         24/1331
   6:     0.18181818    0.18181818    0.18181818          8/1331
   7:     0.27272727    0.18181818    0.09090909         24/1331
   8:     0.27272727    0.18181818    0.18181818         24/1331
   9:     0.27272727    0.27272727    0.00000000          6/1331
          ...
  55:     0.45454545    0.45454545    0.45454545          8/1331

Wave functions: Plane wave expansion
  Cutoff energy: 500.000 eV
  Number of coefficients (min, max): 298, 307
  Pulay-stress correction: 0.000000 eV/Ang^3 (de/decut=0.000000)
  Using FFTW library
  ScaLapack parameters: grid=1x1, blocksize=None 

Eigensolver
   Davidson(niter=1, smin=None, normalize=True) 

Densities:
  Coarse grid: 12*12*12 grid
  Fine grid: 24*24*24 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: pulay
  Linear mixing parameter: 0.05
  Mixing with 5 old densities
  Damping of long wave oscillations: 50 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 24*24*24 grid
  Using the RPBE Exchange-Correlation functional
 

Memory estimate:
  Process memory now: 81.43 MiB
  Calculator: 2.24 MiB
    Density: 0.71 MiB
      Arrays: 0.34 MiB
      Localized functions: 0.23 MiB
      Mixer: 0.13 MiB
    Hamiltonian: 0.23 MiB
      Arrays: 0.22 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.00 MiB
    Wavefunctions: 1.30 MiB
      Arrays psit_nG: 0.59 MiB
      Eigensolver: 0.08 MiB
      Projections: 0.03 MiB
      Projectors: 0.49 MiB
      PW-descriptor: 0.11 MiB

Total number of cores used: 4
Parallelization over k-points: 4

Number of atoms: 1
Number of atomic orbitals: 9
Number of bands in calculation: 9
Bands to converge: occupied states only
Number of valence electrons: 11

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    yes    0.000000    1.805000    1.805000    12     0.1737
  2. axis:    yes    1.805000    0.000000    1.805000    12     0.1737
  3. axis:    yes    1.805000    1.805000    0.000000    12     0.1737

  Lengths:   2.552655   2.552655   2.552655
  Angles:   60.000000  60.000000  60.000000

Effective grid spacing dv^(1/3) = 0.1895

                
                
                
       Cu       
                
                
                
                

Positions:
   0 Cu     0.000000    0.000000    0.000000    ( 0.0000,  0.0000,  0.0000)

Timing:           incl.     excl.
----------------------------------------
PWDescriptor:     0.002     0.002   0.3% |
Set symmetry:     0.110     0.110  16.7% |------|
Other:            0.548     0.548  83.0% |--------------------------------|
----------------------------------------
Total:                      0.661 100.0%

