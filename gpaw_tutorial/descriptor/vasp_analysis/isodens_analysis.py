from ase.io import *
from ase.calculators.vasp import VaspChargeDensity as VCD
from ase.units import Bohr
import sys
#------------------desc modules-----------------------------
#directory where the descriptor modules are 
sys.path.insert(0,'/nfs/home/chliu/descriptor/modules/')
from isodens_surf import *
#----------------------------------------------------------

vdens = VCD(filename='CHGCAR')
dens = vdens.chg[0]#unit: A^-3 total density
atoms = vdens.atoms[0]
desc = VCD(filename='LOCPOT').chg[0]#descriptor

with open('isosurf_info.txt','w') as f:
    f.write('#atomic index, min coord, min val, max coord, max val\n')

isosurf, min_list, max_list = Isodens_surf(atoms,dens,desc,isovalue=4e-3/Bohr**3, tol = 6e-4/Bohr**3,pbc = (0,0,0))#note the unit conversion from Bohr^-3 to A^-3, check pbc!
write('isosurf_vasp.cube', atoms,data = isosurf)
for i in range(len(atoms)):
    with open('isosurf_info.txt','a') as f:
        f.write('%d  (%.3f, %.3f, %3f), %.4e (%.3f, %.3f, %3f), %.4e\n'%(i,min_list[i][0][0],min_list[i][0][1],min_list[i][0][2],min_list[i][1],max_list[i][0][0],max_list[i][0][1],max_list[i][0][2],max_list[i][1]))
