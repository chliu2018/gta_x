from gpaw import *
from ase import *
from ase.io import *
from ase.build import fcc111
from ase.units import Bohr
import sys
#------------------desc modules-----------------------------
sys.path.insert(0,'/nfs/home/chliu/descriptor/modules/')
from descriptor import *
#----------------------------------------------------------

names = ['Au','Ag','Cu','Pt','Pd','Ni','Ir','Rh']
lps = [4.17,4.15,3.63,3.97,3.94,3.53,3.87,3.84]#lattice parameters with PBE
surf = '111'
task = 'desc'
kpts = {'density':2.5,'gamma':True}
kpts1 = {'density':6.0,'gamma':True}
xc = 'PBE'

def calculator(ni_sp = False):
    calc = GPAW(h = 0.2, #grid space 0.2 A
                nbands = 'nao',
                kpts = kpts,
                xc = xc,
                #symmetry = {'point_group':False}, #space symmetry
                txt = "%s_gpaw.txt"%proj,
                #poissonsolver={'dipolelayer': 'xy'},#dipole layer correction
                mixer = Mixer(0.1, 5, weight=100.0),
                occupations=FermiDirac(0.1)
    ) 
    if ni_sp:
        calc.set(spinpol=True,
                 mixer=MixerSum(beta=0.1, nmaxold=5,  weight=100),
                 occupations=FermiDirac(0.1,fixmagmom = False))
     
    return calc

for i,name in enumerate(names):
    proj = "%s_%s_%s"%(name,surf,task)
    slab = fcc111(name,size = (2,2,4),a = lps[i],vacuum = 7.0)
    if name == 'Ni':
        slab.set_initial_magnetic_moments([0.8]*len(slab))
    slab.pbc = (1,1,0)
    calc = calculator(name == 'Ni')
    slab.set_calculator(calc)
    e_f = slab.get_potential_energy()

    calc.set(txt = "%s_harris.txt"%proj,
             symmetry = {'point_group':False}, #space symmetry
             fixdensity = True,
             kpts = kpts1
    )
    e_f = slab.get_potential_energy()
    
    desc = KSDescriptor(calc)
    rho = desc.get_electron_density()
    write('rho_%s.cube'%proj,slab,data = rho*Bohr**3)
    rho = None
    
    vr = desc.electrostatic_potential() - desc.reference_potential()
    write('vr_%s.cube'%proj,slab,data = vr)
    vr = None
    
    Ir = desc.density_descriptor(mode='AIP')
    write('Ir_%s.cube'%proj,slab,data = Ir)
    Ir = None
    
    Er = desc.density_descriptor(mode='MEA')
    write('Er_%s.cube'%proj,slab,data = Er)
    Er = None
