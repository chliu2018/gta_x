#!/bin/bash
#SBATCH --mail-type=ALL
#SBATCH --mail-user=kentmwz@gmail.com
#SBATCH -n 35
#SBATCH -t 12:00:00
#SBATCH -A SNIC2019-1-31
#SBATCH -J fcc_tm

source /proj/hetcalx/users/x_chliu/bash_files/load_gpaw.201908.bash
mpprun gpaw-python fcc_tm.py  
