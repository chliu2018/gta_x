from ase.io.cube import read_cube_data
from ase.io import read, write
from ase.units import Bohr
import sys
#------------------desc modules-----------------------------
#directory where the descriptor modules are 
sys.path.insert(0,'/nfs/home/chliu/descriptor/modules/')
from isodens_surf import *
#----------------------------------------------------------

names = ['no2c6h5','co','h2o','co2','c6h6','ch3oh','ch4','c2h4']
surf = 'gas'
task = 'desc'

with open('isosurf_info.txt','w') as f:
    f.write('#atomic index, min coord, min val, max coord, max val\n')

for name in names:
    proj = "%s_%s_%s"%(name,surf,task)
    with open('isosurf_info.txt','a') as f:
        f.write('-----------------%s-------------------\n'%proj)
    dens, atoms = read_cube_data('rho_%s.cube'%proj)
    for descriptor in ['vr','Er','Ir']:
        with open('isosurf_info.txt','a') as f:
            f.write('===========%s==========\n'%descriptor)
        desc, atoms = read_cube_data('%s_%s.cube'%(descriptor,proj))
        isosurf, min_list, max_list = Isodens_surf(atoms,dens,desc,isovalue=4e-3, tol = 6e-4,pbc = (0,0,0))
        write('isosurf_%s_%s.cube'%(descriptor,proj),atoms,data = isosurf)
        for i in range(len(atoms)):
            with open('isosurf_info.txt','a') as f:
                f.write('%d  (%.3f, %.3f, %3f), %.4e (%.3f, %.3f, %3f), %.4e\n'%(i,min_list[i][0][0],min_list[i][0][1],min_list[i][0][2],min_list[i][1],max_list[i][0][0],max_list[i][0][1],max_list[i][0][2],max_list[i][1]))
