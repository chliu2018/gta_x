from gpaw import *
from ase import *
from ase.io import *
from ase.units import Bohr
import sys
#------------------desc modules-----------------------------
#directory where the descriptor modules are 
sys.path.insert(0,'/nfs/home/chliu/descriptor/modules/')
from descriptor import *
#----------------------------------------------------------

names = ['no2c6h5','co','h2o','co2','c6h6','ch3oh','ch4','c2h4']
surf = 'gas'
task = 'desc'
xc = 'PBE'

def calculator():
    return GPAW(h = 0.2, #grid space 0.2 A
                nbands = 'nao',
                xc = xc,
                symmetry = {'point_group':False}, #space symmetry
                txt = "%s_gpaw.txt"%proj,
    )

for name in names:
    proj = "%s_%s_%s"%(name,surf,task)
    slab = read('../geo/%s.traj'%name)
    calc = calculator()
    slab.set_calculator(calc)
    e_f = slab.get_potential_energy()
    desc = KSDescriptor(calc)
    
    rho = desc.get_electron_density()
    write('rho_%s.cube'%proj,slab,data = rho*Bohr**3)
    rho = None
    
    vr = desc.electrostatic_potential() - desc.reference_potential()
    write('vr_%s.cube'%proj,slab,data = vr)
    vr = None
    
    Ir = desc.density_descriptor(mode='AIP')
    write('Ir_%s.cube'%proj,slab,data = Ir)
    Ir = None

    Er = desc.density_descriptor(mode='MEA')
    write('Er_%s.cube'%proj,slab,data = Er)
    Er = None
