from gpaw import *
from ase.io import *
from ase.visualize import *
from ase.constraints import *
from ase.optimize import *
from ase import *
from ase.optimize.minimahopping import MinimaHopping
from math import *
from gpaw.utilities import h2gpts

name = 'mix'
surf = 'cu100'
task = 'mh'
kpts = (1,1,1)
xc = 'RPBE'

slab = read('geo_end.xyz')

pos = slab.get_positions()

#change m_H to deuterium, 2.01410
slab_masses=slab.get_masses()
for i in range(len(slab)):
    if slab[i].symbol == 'H':
        slab_masses[i]=2.01410

slab.set_masses(slab_masses)

c = []
c.append(FixAtoms(mask=[atom.tag > 2 for atom in slab]))
#spring of COs
for i in range(73,77,2):
    c.append(Hookean(a1=i, a2=i+1,rt=1.58,k=10.))
    c.append(Hookean(a1=i+1, a2=(0,0,1,-(2.5+pos[38][2])),rt=2.2,k=10.))

#spring of H2Os
for i in range(48,72,3):
    c.append(Hookean(a1=i, a2=i+1,rt=1.40,k=5.))
    c.append(Hookean(a1=i, a2=i+2,rt=1.40,k=5.))
    c.append(Hookean(a1=i, a2=(0,0,1,-(8.+pos[38][2])),rt=1.40,k=5.))

#spring of c-c
#c.append(Hookean(a1=32, a2=34,rt=1.58,k=10.))

slab.set_constraint(c)

mixer = Mixer(0.1, 5, weight = 100.0)
#LCAO mode, quick sampling
calc = GPAW(mode = 'lcao',
            kpts = kpts,
            basis = 'dzp',
            xc = xc,
            txt = '%s_%s_%s_gpaw.out' % (name,surf,task),
            symmetry = {'point_group':False},
            mixer = mixer,
            maxiter = 250,
            occupations = FermiDirac(0.1),
            parallel = {'sl_auto':True, 'use_elpa':True}#specific to LCAO mode!
)

slab.set_calculator(calc)

hop = MinimaHopping(slab,Ediff0=0.75,T0=2000,fmax=0.05,timestep=1.0,optimizer=QuasiNewton)
hop(totalsteps=20)

e_f = slab.get_potential_energy()
f = open('Energy_%s_%s_%s.txt' % (name,surf,task), 'w')
f.write('%s for %s: %.3f eV' % (name, surf, e_f))
f.close()
