from gpaw import *
from ase import *
from ase.io import *
from ase.build import fcc111
from ase.units import Bohr

name = 'Ni'
a = 3.53
surf = '111'
task = 'wf'
xc = 'PBE'
kpts = {'density':2.5,'gamma':True}
proj = '%s_%s_%s'%(name,surf,task)


slab = fcc111(name,size = (2,2,4),a = a,vacuum = 7.0)
slab.set_initial_magnetic_moments([0.8]*len(slab))
calc = GPAW(h = 0.2, #grid space 0.2 A
            nbands = 'nao',
            kpts = kpts,
            xc = xc,
            #symmetry = {'point_group':False}, #space symmetry
            txt = "%s_gpaw.txt"%proj,
            #poissonsolver={'dipolelayer': 'xy'},#dipole layer correction
            mixer = Mixer(0.1, 5, weight=100.0),
            occupations=FermiDirac(0.1)
) 
calc.set(spinpol=True,
         mixer=MixerSum(beta=0.1, nmaxold=5,  weight=100),
         occupations=FermiDirac(0.1,fixmagmom = False))
slab.set_calculator(calc)
e_f = slab.get_potential_energy()
calc.write('%s.gpw'%proj)
