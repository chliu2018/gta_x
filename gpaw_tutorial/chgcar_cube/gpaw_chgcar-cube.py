from ase.units import Bohr
from ase.io import *
from gpaw import *
from ase.calculators.vasp import VaspChargeDensity as VCD

name = 'ni111'
atoms, calc = restart('Ni_111_wf.gpw',txt=None)

rho = atoms.calc.get_all_electron_density(gridrefinement=4)
write('density_%s.cube'%name, atoms, data=rho * Bohr**3)#unit: Bohr^-3

vasp_dens = VCD(filename=None)
vasp_dens.atoms.append(atoms)
vasp_dens.chg.append(rho)
vasp_dens.write(filename='CHGCAR_%s_0'%name,format = 'chgcar')#unit: Å^-3

vasp_dens.chgdiff.append(calc.get_all_electron_density(spin = 0, gridrefinement=4) - calc.get_all_electron_density(spin = 1, gridrefinement=4))
vasp_dens.write(filename='CHGCAR_%s_1'%name,format = 'chgcar')#unit: Å^-3
