from ase.units import Bohr
from ase.io.cube import read_cube_data
from ase.calculators.vasp import VaspChargeDensity as VCD

name = 'ni111'
data, atoms = read_cube_data('density_%s.cube'%name)
vasp_dens = VCD(filename=None)
vasp_dens.atoms.append(atoms)
vasp_dens.chg.append(data/Bohr**3)
vasp_dens.write(filename='CHGCAR_%s_c2v'%name,format = 'chgcar')
