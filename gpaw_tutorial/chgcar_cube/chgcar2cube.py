from ase.io import *
from ase.calculators.vasp import VaspChargeDensity as VCD
from ase.units import Bohr

name = 'ni111'
vasp_dens = VCD(filename='CHGCAR_%s_1'%name)
#write on frame to one cube file, in this case frame 0
write('density_%s_v2c.cube'%name, images=vasp_dens.atoms[0], data=vasp_dens.chg[0]*Bohr**3)#unit: Bohr^-3
