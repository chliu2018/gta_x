#!/bin/bash
#SBATCH --mail-type=ALL
#SBATCH --mail-user=kentmwz@gmail.com
#SBATCH -D .
#SBATCH -n 16
#SBATCH -t 2:00:00
#SBATCH -J ni111_gen

export OMP_NUM_THREADS=1
export OMPI_MCA_orte_process_binding=core
export OMPI_MCA_rmaps_base_mapping_policy=core

source /nfs/home/chliu/bash_files/load_gpaw_1908.bash
mpirun gpaw-python ni111_gen.py
