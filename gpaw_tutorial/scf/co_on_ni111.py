from ase.build import fcc111,add_adsorbate,molecule
from gpaw import *
from ase import *

name = 'co'
surf = 'ni111'
task = 'scf'
kpts = {'density':2.5,'gamma':True}#kpt density in 1BZ: 2.0 kpts per Å^-1, including Gamma point
xc = 'RPBE'

slab = fcc111('Ni',size = (2,2,4),vacuum = 8.0)
slab.pbc = (1,1,0)
molecule = molecule('CO')

def magmom_ini_set(atoms):
    spin_list = [0.0]*len(atoms)
    for m in range(len(atoms)):
        if atoms[m].symbol == 'Ni':
            spin_list[m] = 0.8
    atoms.set_initial_magnetic_moments(spin_list)

add_adsorbate(slab,molecule,1.95,'ontop',mol_index = 1)
#set initial magmom for all Ni atoms
magmom_ini_set(slab)

calc = GPAW(mode = PW(350),#plane wave mode, 350 eV cutoff for wavefunction
            kpts = kpts,
            xc = xc,
            maxiter = 1000, #spin-pol calculations sometimes need more steps
            symmetry = {'point_group':False}, #space symmetry
            txt = "%s_%s_%s_gpaw.txt"%(name,surf,task),
            mixer = MixerSum(0.1, 5, weight=100.0), #MixerSum for spinpol calculation
            occupations=FermiDirac(0.1,fixmagmom = False),
            spinpol = True
)

slab.set_calculator(calc)

e_f = slab.get_potential_energy()

f = open('Energy_%s_%s_%s.txt'%(name,surf,task),'w')
f.write('Final energy: %.3f\n'%e_f)
f.close()
