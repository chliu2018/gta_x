from ase.io import *
from gpaw import *

proj = 'co_gs'
atoms = read('co_opt.traj@-1')
calc = GPAW(h = 0.2,
            xc = "RPBE",
            txt = "%s_gpaw.txt"%proj
)

atoms.set_calculator(calc)

e_gs = atoms.get_potential_energy()+calc.get_reference_energy()

calc.set(setups = {0:'fch1s'},txt='co_exc_o_gpaw.txt',charge= 0,spinpol = True,occupations=FermiDirac(0.0, fixmagmom=True))
e_o = atoms.get_potential_energy()+calc.get_reference_energy()

calc.set(setups = {1:'fch1s'},txt='co_exc_c_gpaw.txt',charge= 0,spinpol = True,occupations=FermiDirac(0.0, fixmagmom=True))
e_c = atoms.get_potential_energy()+calc.get_reference_energy()

with open('xps_fin.txt','w') as f:
    f.write('O0: %.2f, C1: %.2f\n'%(e_o-e_gs,e_c-e_gs))
