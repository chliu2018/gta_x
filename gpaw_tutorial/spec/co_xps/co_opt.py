from ase.build import molecule
from gpaw import *
from ase.optimize import QuasiNewton

proj = 'co_opt'

atoms = molecule('CO')
atoms.set_cell([15]*3)
atoms.center()

calc = GPAW(h = 0.2,
            xc = "RPBE",
            txt = "%s_gpaw.txt"%proj
)

atoms.set_calculator(calc)
dyn = QuasiNewton(atoms,logfile = "%s.log"%proj,trajectory='%s.traj'%proj)
dyn.run(fmax = 0.001)

e_f = atoms.get_potential_energy()
with open('E_%s.txt'%proj,'w') as f:
    f.write("%.3"%e_f)
