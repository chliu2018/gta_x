from ase import *
from ase.calculators.calculator import kptdensity2monkhorstpack as k2m
from gpaw import *
from gpaw import setup_paths
from gpaw.xas import XAS, RecursionMethod
from ase.io import *
import sys

name = 'co-ni'
elem = {17:'C'}

name_xes = name + '_xes'

def kpts_2D(atoms,density):
    kpts_size = k2m(atoms,density,even=False)
    kpts_size[2] = 1
    return kpts_size

def magmom_ini_set(atoms):
    spin_list = [0.0]*len(atoms)
    for m in range(len(atoms)):
        if atoms[m].symbol == 'Ni':
            spin_list[m] = 0.8
    atoms.set_initial_magnetic_moments(spin_list)    

atoms = read('%s.traj'%name)#for XES we use original small cell
atoms.pbc = 1

kpts = {'size':kpts_2D(atoms,2.5),'gamma':True} 
kpts1 = {'size':kpts_2D(atoms,6.0),'gamma':True}

magmom_ini_set(atoms)

mixer=Mixer(beta=0.01, nmaxold=5,  weight=100)
mixersum=MixerSum(beta=0.01, nmaxold=5,  weight=100)

#xes calculation gs approx.
for carbon in [17]:
    calc= GPAW( h=0.2,
                kpts = kpts,
                txt = name_xes + '_%s%d.0.txt'%(elem[carbon],carbon),
                xc='RPBE',
                maxiter=1000,
                occupations=FermiDirac(0.1,fixmagmom=False),                
                mixer=mixersum,
                spinpol = True,
                setups={carbon:'xes1s'}
    )

    atoms.set_calculator(calc)
    
    e_gs = atoms.get_potential_energy() + calc.get_reference_energy()
    e_fermi = calc.get_fermi_level()

    calc.set(txt=name_xes + '_%s%d.txt'%(elem[carbon],carbon),
             symmetry = {'point_group':False}, #space symmetry
             fixdensity = True,
             kpts = kpts1)
    e_f = calc.get_potential_energy()
    
    calc.write(name_xes + '_%s%d.gpw'%(elem[carbon],carbon))
