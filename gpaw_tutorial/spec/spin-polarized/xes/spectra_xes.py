import numpy as np
import pylab as plt
from scipy.interpolate import InterpolatedUnivariateSpline
from cycler import *

name = 'co-ni'
carbon = 17

elem = {17:'C'}
pol_list = ['tot','pxy','pz']

def avg_xes(x_0,y_0,x_1,y_1):
    x = np.linspace(min(x_0[0],x_1[0]),max(x_0[-1],x_1[-1]),max(len(x_0),len(x_1)))
    spl0 = InterpolatedUnivariateSpline(x_0,y_0)
    spl1 = InterpolatedUnivariateSpline(x_1,y_1)
    y = (spl0(x)+spl1(x))/2.
    return x,y

for j,pol in enumerate(pol_list):
    plt.figure(j+1)

    data_0 = np.loadtxt(name + '_xes_%s%d_0.dat'%(elem[carbon],carbon))#important!
    data_1 = np.loadtxt(name + '_xes_%s%d_1.dat'%(elem[carbon],carbon))
    x,y = avg_xes(data_0[:,0],data_0[:,1],data_1[:,0],data_1[:,1])
    plt.plot(x, y, label=(name))

    plt.title('XES_%s_%s%d_%s'%(name,elem[carbon],carbon,pol))
    plt.xlabel('Energy(eV)')
    plt.ylabel('Intensity(a.u.)')
    plt.legend(prop={'size':8},loc = 'best')
    plt.xlim(-5,20)
    plt.savefig('XES_%s_%s%d_%s.png'%(name,elem[carbon],carbon,pol, dpi=160)
    plt.close()
            
