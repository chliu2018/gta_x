import ase
import ase.build

slab = ase.build.fcc100('Ni',size=(2,2,4),vacuum=7.)
molecule = ase.build.molecule('CO')
ase.build.add_adsorbate(slab,molecule,1.80,'hollow',mol_index=1)
slab.center(axis=2,vacuum=7.)
ase.io.write('geo_co-ni.traj',slab)
