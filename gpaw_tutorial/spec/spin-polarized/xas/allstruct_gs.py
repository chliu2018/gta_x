from ase.io import *
from ase.calculators.calculator import kptdensity2monkhorstpack as k2m
from gpaw import *
import numpy as np

name = 'co-ni'

def kpts_2D(atoms,density):
    kpts_size = k2m(atoms,density,even=False)
    kpts_size[2] = 1
    return kpts_size

def magmom_ini_set(atoms):
    spin_list = [0.0]*len(atoms)
    for m in range(len(atoms)):
        if atoms[m].symbol == 'Ni':
            spin_list[m] = 0.8
    atoms.set_initial_magnetic_moments(spin_list)    

name_gs = name + '_gs'
name_xas = name + '_xas'
name_xps = name + '_xps'
name_xes = name + '_xes'
xc = 'RPBE'

#cell is already orthorhombic
atoms_0= read('%s.traj'%name)
atoms_0.set_constraint()
atoms = atoms_0*(2,2,1)
atoms.pbc = 1

kpts = {'size':kpts_2D(atoms,2.5),'gamma':True}

magmom_ini_set(atoms)

mixer=Mixer(beta=0.1, nmaxold=5,  weight=100)
mixersum=MixerSum(beta=0.01, nmaxold=5,  weight=100)

#gs calc
calc= GPAW( h=0.2,
            kpts = kpts,
            txt = name_gs + '.txt',
            xc = xc,
            maxiter=1000,
            occupations=FermiDirac(0.1,fixmagmom = False),
            #symmetry = {'point_group':False}, #space symmetry
            mixer=mixersum,
            spinpol = True
)

atoms.set_calculator(calc)

e_gs = atoms.get_potential_energy() + calc.get_reference_energy()
e_fermi = calc.get_fermi_level()

calc.write(name_gs + '.gpw')
