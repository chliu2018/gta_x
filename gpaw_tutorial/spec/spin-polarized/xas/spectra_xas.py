import numpy as np
import pylab as plt
from cycler import *
from scipy.interpolate import InterpolatedUnivariateSpline

name = 'co-ni'
#x and y limits
x_1 = {'C':280,'O':520}
x_2 = {'C':320,'O':550}
carbon = 17

elem = {17:'C'}
pol_list = ['tot','pxy','pz']

def avg_xes(x_0,y_0,x_1,y_1):
    x = np.linspace(min(x_0[0],x_1[0]),max(x_0[-1],x_1[-1]),max(len(x_0),len(x_1)))
    spl0 = InterpolatedUnivariateSpline(x_0,y_0)
    spl1 = InterpolatedUnivariateSpline(x_1,y_1)
    y = (spl0(x)+spl1(x))/2.
    return x,y

for j,pol in enumerate(pol_list):
    plt.figure(j+1)
    
    data = np.loadtxt(name + '_xas_%s%d_0.dat'%(elem[carbon],carbon))#important!
    data_1 = np.loadtxt(name + '_xas_%s%d_1.dat'%(elem[carbon],carbon))
    x, y= avg_xes(data[:,0],data[:,j+1],data_1[:,0],data_1[:,j+1])
    plt.plot(x, y, label=(pol))
    
    plt.title('XAS_%s%d_%s'%(name,carbon,pol))
    plt.xlabel('Energy(eV)')
    plt.ylabel('Intensity(a.u.)')
    plt.legend(prop={'size':8},loc = 'best')
    plt.xlim(x_1[elem[carbon]],x_2[elem[carbon]])
    
    plt.savefig('XAS_%s_%s%d_%s.png'%(name,elem[carbon],carbon,pol, dpi=160)
    plt.close()
