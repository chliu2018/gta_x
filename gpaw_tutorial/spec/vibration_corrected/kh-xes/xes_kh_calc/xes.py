import os
from math import pi, cos, sin
from ase import *
from gpaw import *
from gpaw.atom.generator import *
from gpaw.xas import XAS, RecursionMethod
import numpy as npy
import sys
from ase.io import *
from ase.constraints import *

names = ['ptb_%d'%(i+1) for i in range(3)] + ['Ni-C_p4g']
task = 'xes'
c_ind = [[65]]*4
xc = 'RPBE'
kpts = {'size':(2,2,1),'gamma':True}
kpts1 = {'size':(4,4,1),'gamma':True} 
dlist = [npy.linspace(-0.5,1.5,41)]*4

for name in names:
    i = names.index(name)
    for d in dlist[i]:
        atoms = read('../geo/%s.traj'%name) # must match xyzfile in submit_all.bash
        atoms[65].position[2] += d
    
        spin_list = [0.0]*len(atoms)
        for j in range(len(atoms)):
            if atoms[j].symbol == 'Ni':
                spin_list[j] = 0.8
        atoms.set_initial_magnetic_moments(spin_list)

        mixer=Mixer(beta=0.01, nmaxold=5,  weight=100)
        #mixersum=MixerSum(beta=0.01, nmaxold=5,  weight=100)
        mixersum=MixerSum(beta=0.01, nmaxold=1,  weight=100)#only for the last one
        
        for carbon in c_ind[i]:
            if os.path.isfile('%s_%s_%.2f_C%d.gpw'%(name,task,d,carbon)) == False:
                calc= GPAW( h=0.2,
                            txt = '%s_%s_%.2f_C%d.txt'%(name,task,d,carbon),
                            nbands = 'nao',
                            xc=xc,
                            kpts = kpts,
                            maxiter=1000,
                            #stencils=(3,3),
                            poissonsolver=PoissonSolver(nn=3,eps=1e-12),
                            #symmetry={'point_group':False},
                            eigensolver=Davidson(3),
                            occupations=FermiDirac(0.2, fixmagmom=False),
                            spinpol=True,
                            mixer=mixersum,
                            setups={carbon:'xes1s'})

                atoms.set_calculator(calc)

                e_gs = atoms.get_potential_energy() + calc.get_reference_energy()
                e_fermi = calc.get_fermi_level()

                calc.set(txt='%s_%s_%.2f_C%d_harris.txt'%(name,task,d,carbon),
                         fixdensity = True,
                         kpts=kpts1)
                calc.get_potential_energy()

                calc.write('%s_%s_%.2f_C%d.gpw'%(name,task,d,carbon))

