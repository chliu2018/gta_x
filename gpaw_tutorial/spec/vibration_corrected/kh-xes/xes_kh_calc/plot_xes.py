import os
from math import pi, cos, sin
from ase import *
from ase.parallel import rank, barrier
from gpaw import *
from gpaw.atom.generator import *
from gpaw import setup_paths
#from gpaw.xas import XAS
#from pylab import *
import numpy as npy
import sys
sys.path.insert(0,'/nfs/home/chliu/gpaw-ase/gpaw-1.5.2-xas-dipolemom/lib/python2.7/site-packages/gpaw/')
from xas import *

#setup_paths.insert(0, '.')

names = ['ptb_%d'%(i+1) for i in range(3)] + ['Ni-C_p4g']
path = './'
#dscf = [283.610206,283.518062,283.769958,284.632485]
#E_in = np.linspace(2.340385589599609375e+02,3.304380798339843750e+02,num=317)
fwhm = 2.0#eV
d_list = [npy.linspace(-0.5,1.5,41)]*4

for name in names:
    i = names.index(name)
    for d in d_list[i]:
        for spin in [0,1]:
            calc = GPAW('%s%s_xes_%.2f_C65.gpw'%(path,name,d), txt=None, idiotproof=False)
            xas = XAS(calc, mode='xes',spin=spin)
    
            x, y = xas.get_spectra(stick = True)
            X = npy.zeros((len(x),4))
            ef=calc.get_fermi_levels_mean()
            X[:,0] = x - ef # for KH
            X[:,1] = y[0,:]
            X[:,2] = y[1,:]
            X[:,3] = y[2,:]
            f = open('%s_%.2f_C65.%d.dat'%(name,d,spin),'w')
            f.write('#Sticks, transition moments, x, y, z\n')
            f.close()
            with open('%s_%.2f_C65.%d.dat'%(name,d,spin),'a') as f_handle:
                npy.savetxt(f_handle, X)
                f_handle.close()
