import numpy as np
from scipy.special import wofz

fwhm = 3.7 #Gaussian broadening
fwhm2 = fwhm # Gaussian broadening 2
width = 0.0 #Lorentzian broadening
#output range
begin = 250
end = 300
dx = 0.02

names = ['ptb_%d'%(i+1) for i in range(3)] + ['Ni-C_p4g']

def get_intensity(x_in,y_in,x_out,linbroad):
    total = np.zeros_like(x_out)
    sigma = np.zeros_like(x_in)
    sigma = np.where(x_in < linbroad[1],fwhm/(2*np.sqrt(2 * np.log(2))),(fwhm+(x_in-linbroad[1])/(linbroad[2]-linbroad[1])*(linbroad[0]-fwhm))/(2*np.sqrt(2 * np.log(2))))
    sigma = np.where(x_in <= linbroad[2],sigma, linbroad[0]/(2*np.sqrt(2 * np.log(2))))
    for i,x in enumerate(x_in):
        voi_b = np.real(wofz(((x_out-x) + 1j*width)/sigma[i]/np.sqrt(2))) / sigma[i] / np.sqrt(2*np.pi)
        total += y_in[i]*voi_b
    return total

for name in names:
    for spin in [0,1]:
        data = np.loadtxt('../../xes_kh_gen/kh_inp_%s_%d/%s_%d_XES_sigma.dat'%(name,spin,name,spin))
        x_in,y_in = data[:,0],data[:,1]
        x_out = begin + np.arange(0,end-begin, dx)
        y_out = get_intensity(x_in,y_in,x_out,[fwhm2,x_in[0],x_in[0]+20])
        np.savetxt('%s_%d_XES_sigma_brd.dat'%(name,spin),list(zip(x_out,y_out)))
