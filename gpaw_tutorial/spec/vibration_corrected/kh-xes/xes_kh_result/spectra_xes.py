import numpy as np
import pylab as plt

names_t = ['ptb_%d'%(i+1) for i in range(3)]+['Ni-C_p4g']
names_e = ['-0.5','0.2','1.2','1.2']

x_a = 270.
x_b = 290.
fitc = np.loadtxt('../../fc-xas/xas_fc_result/fit_coef_new.dat')
c_shift = 10.178

num_plots=len(names_t)
colormap = plt.cm.get_cmap('rainbow')
color_list = np.linspace(0.0,1.0,num_plots)

data_t = []
for name_t in names_t:
    data = np.loadtxt('theo/%s_0_XES_sigma_brd.dat'%name_t)
    data_1 = np.loadtxt('theo/%s_1_XES_sigma_brd.dat'%name_t)
    x = data[:,0]
    X = np.zeros((len(x),2))
    X[:,0] = x
    X[:,1] = (data[:,1]+data_1[:,1])/2
    data_t.append(X)

x_1 = np.loadtxt('exp/Fig1_XES_energy.txt')
for i in range(len(names_e)):
    name_e = names_e[i]
    y_1 = np.loadtxt('exp/Fig1_XES_%sps.txt'%name_e)
    x_2 = data_t[0][:,0]+c_shift
    y_2 = np.zeros_like(x_2)
    for j in range(len(fitc[i])):
        y_2 += fitc[i][j]*data_t[j][:,1]
    mask = np.array([x >= x_a and x <= x_b for x in x_1])
    mask_2 = np.array([x >= x_a and x <= x_b for x in x_2])
    x_1m = x_1[mask]
    y_1m = y_1[mask]
    x_2m = x_2[mask_2]
    y_2m = y_2[mask_2]
    y_2 *= np.trapz(y_1m,x_1m)/np.trapz(y_2m,x_2m)
    plt.figure(i+1)
    plt.plot(x_1m,y_1m,label='exp')
    plt.plot(x_2,y_2,label='KH')
    plt.xlim(x_a,x_b)
    plt.title('XES, %sps'%name_e)
    plt.xlabel('Energy(eV)')
    plt.ylabel('Intensity(a.u.)')
    plt.minorticks_on()
    plt.legend(prop={'size':8},loc = 'upper right')
    if i == 3:
        plt.savefig("xes_%s-4p0.png"%name_e.replace('.','p'),dpi=150)
    else:
        plt.savefig("xes_%s.png"%name_e.replace('.','p'),dpi=150)
    plt.close()

plt.figure(5)
for name_e in [names_e[0],names_e[1]]: 
    y_1 = np.loadtxt('exp/Fig1_XES_%sps.txt'%name_e)
    x_2 = data_t[0][:,0]+c_shift
    y_2 = data_t[3][:,1]
    mask = np.array([x >= x_a and x <= x_b for x in x_1])
    mask_2 = np.array([x >= x_a and x <= x_b for x in x_2])
    x_1m = x_1[mask]
    y_1m = y_1[mask]
    x_2m = x_2[mask_2]
    y_2m = y_2[mask_2]
    y_2 *= np.trapz(y_1m,x_1m)/np.trapz(y_2m,x_2m)
    plt.figure(i+1)
    plt.plot(x_1m,y_1m,label='exp')
    plt.plot(x_2,y_2,label='KH')
    plt.xlim(x_a,x_b)
    plt.title('XES, %sps'%name_e)
    plt.xlabel('Energy(eV)')
    plt.ylabel('Intensity(a.u.)')
    plt.minorticks_on()
    plt.legend(prop={'size':8},loc = 'upper right')
    plt.savefig("xes_%s_0.png"%name_e.replace('.','p'),dpi=150)
    plt.close()
#------------------------------------------------------
data_t = []

for name_t in names_t:
    data_0 = np.loadtxt('theo/%s_0_XES_sigma_brd.dat'%name_t)
    data_1 = np.loadtxt('theo/%s_1_XES_sigma_brd.dat'%name_t)
    x,y = data_0[:,0], (data_0[:,1]+data_1[:,1])/2.
    X = np.zeros((len(x),2))
    X[:,0]=x
    X[:,1]=y
    data_t.append(X)

plt.figure(6)
for i in [0]:
    name_e = names_e[i]
    y_1 = np.loadtxt('exp/Fig1_XES_%sps.txt'%name_e)
    mask = np.array([x >= x_a and x <= x_b for x in x_1])
    x_1m = x_1[mask]
    y_1m = y_1[mask]
    plt.plot(x_1m,y_1m,label='exp, %sps'%names_e[i],color = 'k')
    area_1m = np.trapz(y_1m,x_1m)

for i in range(len(names_t)):
    x_2 = data_t[i][:,0]+c_shift
    y_2 = data_t[i][:,1]
    mask_2 = np.array([x >= x_a and x <= x_b for x in x_2])
    x_2m = x_2[mask_2]
    y_2m = y_2[mask_2]
    area_2m = np.trapz(y_2m,x_2m)
    plt.plot(x_2m,y_2m/area_2m*area_1m,label='%s'%names_t[i])
plt.xlim(x_a,x_b)
plt.title('XES, theo')
plt.xlabel('Energy(eV)')
plt.ylabel('Intensity(a.u.)')
plt.minorticks_on()
plt.legend(prop={'size':8},loc = 'upper left')
plt.savefig("xes_theo_avg.png",dpi=150)
plt.close()
#------------------------------------------------------
plt.figure(7)
for i in range(len(names_t)):
    name_t = names_t[i]
    data_0 = np.loadtxt('theo/%s_0_XES_sigma_brd.dat'%name_t)
    data_1 = np.loadtxt('theo/%s_1_XES_sigma_brd.dat'%name_t)
    x_0 = data_0[:,0]
    y_0 = data_0[:,1]
    x_1 = data_1[:,0]
    y_1 = data_1[:,1]
    plt.plot(x_0+c_shift,y_0,label = '%s,0'%name_t,ls = '-',color = colormap(color_list[i]))
    plt.plot(x_1+c_shift,y_1,label = '%s,1'%name_t,ls = '--',color = colormap(color_list[i]))

plt.xlim(x_a,x_b)
plt.title('XES, spin')
plt.xlabel('Energy(eV)')
plt.ylabel('Intensity(a.u.)')
plt.minorticks_on()
plt.legend(prop={'size':8},loc = 'upper left')
plt.savefig("xes_theo_spin.png",dpi=150)
plt.close()
