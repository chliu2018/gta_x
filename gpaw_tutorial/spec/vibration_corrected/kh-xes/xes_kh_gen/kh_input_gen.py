import numpy as np
from ase.units import *
from ase.io import *
import os

def fortran_save(data,filename,fmt):
    np.savetxt(filename,data,fmt = fmt)
    data = np.genfromtxt(filename,dtype=str)
    data = np.core.defchararray.replace(data,'e','D')
    np.savetxt(filename,data,fmt= "%s")


pol = False #if KH has polarization functionality
names = ['ptb_%d'%(i+1) for i in range(3)] + ['Ni-C_p4g']
dpath = "../xes_kh_calc/"
path_pes = '../../pes'
d_lists = [np.linspace(-0.5,1.5,41)]*4
spins = [0,1]
nibz = 10

fo = open('batch_exe.sh','w')
#fo.write("sckh=/nfs/home/chliu/submission_scripts/sckh_sub.201909.py\n")
fo.write("sckh=/nfs/home/chliu/sckh/sckh/build/sckh_main\n")
for name in names:
    m = names.index(name)
    d_list = d_lists[m]
    for spin in spins:
        sbdir = 'kh_inp_%s_%d'%(name,spin)
        if os.path.isdir('%s'%sbdir) == False:
            os.system('mkdir %s'%sbdir)
    
        f1 = open('%s/pes_file_list_final.dat'%sbdir,'w')
        f2 = open('%s/dipole_file_list.dat'%sbdir,'w')
        ref_e = [0.0,0.0]#gs,exc
        #calc_0 = GPAW("%s/%s_gs_0.00_C65.gpw"%(dpath,name),txt=None)
        #e_0 = calc_0.get_potential_energy()+calc_0.get_reference_energy()
    
        #------------pes_file_initial.dat----------------
        data_gs = np.loadtxt('%s/Energy_%s_pes_gs.txt'%(path_pes,name))
        e_0 = data_gs[np.where(data_gs[:,0] == 0.00)[0][0]][1]
        data_gs[:,1] += ref_e[0]-e_0
        data_gs[:,1] /= Hartree
        mask = np.array([x >= d_list[0] and x <= d_list[-1] for x in data_gs[:,0]])
        fortran_save(data_gs[mask],'%s/pes_file_initial.dat'%sbdir,"%.2f %.10e")

        #------------pes_file_intermediate.dat------------
        data_exc = np.loadtxt('%s/Energy_%s_pes_exc.txt'%(path_pes,name))
        data_exc[:,1] += ref_e[1]-e_0
        data_exc[:,1] /= Hartree
        fortran_save(data_exc[mask],'%s/pes_file_intermediate.dat'%sbdir,"%.2f %.10e")

        #-------------pes_file_final_n.dat & dipole_file_n.dat--------------
        c_1s = -273.374

        data_gs = np.loadtxt('%s/Energy_%s_pes_gs.txt'%(path_pes,name))
        data_gs[:,1] += ref_e[0]-e_0
        data_gs = data_gs[mask]
        data_exc = np.loadtxt('%s/Energy_%s_pes_exc.txt'%(path_pes,name))
        data_exc[:,1] += ref_e[1]-e_0
        data_exc = data_exc[mask]

        dipole_data = []
        for d in d_list:
            dptmp = np.loadtxt('%s/%s_%.2f_C65.%d.dat'%(dpath,name,d,spin))
            dipole_data.append(dptmp)
        dipole_lens = np.array([len(dipl) for dipl in dipole_data])
        dpl_max = np.amax(dipole_lens)
        dpl_min = np.amin(dipole_lens)
        if dpl_max != dpl_min:
            for p in range(len(dipole_data)):
                dpld = dipole_data[p]
                if len(dpld) != dpl_min:
                    mask_d = np.array([k%(len(dpld)/nibz) < dpl_min/nibz for k in range(len(dpld))])
                    dipole_data[p] = dipole_data[p][mask_d]
        nf = len(dipole_data[0]) #make nstates odd!
        for i in range(nf):
            e_f = []
            d_x = []
            d_y = []
            d_z = []
            x = d_list
            X = np.zeros((len(x),2))
            Y = np.zeros((len(x),4))
            for j in range(len(x)):
                e_corr = (data_exc[j][1]-dipole_data[j][i][0]+c_1s)/Hartree
                e_f.append(e_corr)
                d_x.append(dipole_data[j][i][1])
                d_y.append(dipole_data[j][i][2])
                if pol == True:
                    d_z.append(dipole_data[j][i][3])
                else:
                    d_z.append(np.zeros_like(dipole_data[j][i][3]))
            X[:,0] = x
            X[:,1] = np.array(e_f)
            fortran_save(X,"%s/pes_file_final_%d.dat"%(sbdir,i),"%.2f %.10e")
            Y[:,0] = x
            Y[:,1] = np.array(d_x)
            Y[:,2] = np.array(d_y)
            Y[:,3] = np.array(d_z)
            fortran_save(Y,"%s/dipole_file_%d.dat"%(sbdir,i),"%.2f %.10e %.10e %.10e")
            f1.write("pes_file_final_%d.dat\n"%i)
            f2.write("dipole_file_%d.dat\n"%i)
        #------------------input.txt---------------------------------
        f = open('%s/input.txt'%sbdir,'w')
        f.write("""\

runmode KH

pes_file_i pes_file_initial.dat
pes_file_n pes_file_intermediate.dat
pes_file_list_f    pes_file_list_final.dat
dipole_file_list_f dipole_file_list.dat
outfile %s_%d_XES
npesfile_f %d
shift_PES 0
npoints_in %d
nstates %d
mu 12.0107
dvr_start_in %.2f
dx_in 0.05
omega_out_start 250
omega_out_end 300
n_omega_out 501
gamma_FWHM 0.18
use_proj T
nproj 3
proj_file proj_3.dat
"""%(name,spin,nf,len(d_list),len(d_list),d_list[0]))
        f.close()

        f0 = open('%s/proj_3.dat'%sbdir,'w')
        f0.write("""\
1.0 0.0 0.0
0.0 1.0 0.0
0.0 0.0 1.0
""")
        f0.close()
        fo.write("""\
cd %s
${sckh} > sckh_log.out 2>&1 &
cd ..
"""%sbdir)
fo.close()
