import os
from math import pi, cos, sin
from ase import *
from gpaw import *
from gpaw.atom.generator import *
from gpaw import setup_paths
from gpaw.xas import XAS, RecursionMethod
import numpy as npy
import sys
from ase.io import *
from ase.constraints import *

names = ['ptb_%d'%(i+1) for i in range(3)] + ['Ni-C_p4g']
task = 'gs'
carbon = 65
xc = 'RPBE'
d_list = [npy.linspace(-0.5,1.5,41)]*4
kpts = {'size':(2,2,1),'gamma':True}

for name in names:
    j = names.index(name)
    f = open('Energy_%s_pes_%s.txt'%(name,task),'w')
    for d in d_list[j]:
        if os.path.isfile('%s_%s_%.2f_C%d.gpw'%(name,task,d,carbon)) == False:
            atoms = read('../geo/%s.traj'%name)
            spin_list = [0.0]*len(atoms)
            for i in range(len(atoms)):
                if atoms[i].symbol == 'Ni':
                    spin_list[i] = 0.8
            atoms.set_initial_magnetic_moments(spin_list)

            mixer=Mixer(beta=0.01, nmaxold=5,  weight=100)
            #mixersum=MixerSum(beta=0.02, nmaxold=5,  weight=100)
            mixersum=MixerSum(beta=0.01, nmaxold=5,  weight=100)#for the last one only

            atoms[carbon].position[2]+=d
            calc= GPAW( h=0.2,
                        kpts = kpts,
                        txt = '%s_%s_%.2f_gpaw.txt'%(name,task,d),
                        xc=xc,
                        maxiter=1000,
                        poissonsolver=PoissonSolver(nn=3,eps=1e-12),
                        #symmetry={'point_group':False},
                        eigensolver=Davidson(3),
                        occupations=FermiDirac(0.1, fixmagmom=False),
                        spinpol=True,
                        mixer=mixersum)

            atoms.set_calculator(calc)
            e_pot = atoms.get_potential_energy()
            calc.write('%s_%s_%.2f_C%d.gpw'%(name,task,d,carbon))
        else:
            atoms,calc = restart('%s_%s_%.2f_C%d.gpw'%(name,task,d,carbon),txt=None)
        e_gs = calc.get_potential_energy()+calc.get_reference_energy()
        f.write('%.2f %.6f\n'%(d,e_gs))
    f.close()
