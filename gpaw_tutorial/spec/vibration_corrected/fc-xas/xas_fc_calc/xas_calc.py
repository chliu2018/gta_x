from gpaw import *
from gpaw.xas import XAS, RecursionMethod
import numpy as npy
import sys
from ase.io import *
import os

kpts = {'size':(2,2,1),'gamma':True}
kpts1 = {'size':(4,4,1),'gamma':True} 
names = ['ptb_%d'%(i+1) for i in range(3)]+['Ni-C_p4g']
c_ind = [[65]]*4
xc = 'RPBE'
dlist = [npy.linspace(-0.5,1.5,41)]*4

for i in range(len(names)):
    name=names[i]

    for d in dlist[i]:
        name_hch = '%s_%.2f_xas' % (name,d)
        name_exc = '%s_%.2f_exc' % (name,d)
        name_xps = '%s_%.2f_xps' % (name,d)

        atoms = read('../../geo/%s.traj'%name) # must match xyzfile in submit_all.bash
        atoms[65].position[2] += d
    
        spin_list = [0.0]*len(atoms)
        for j in range(len(atoms)):
            if atoms[j].symbol == 'Ni':
                spin_list[j] = 0.8
        atoms.set_initial_magnetic_moments(spin_list)

        mixer=Mixer(beta=0.01, nmaxold=5,  weight=100)
        mixersum=MixerSum(beta=0.01, nmaxold=5,  weight=100)

        #hch calculation,carbon

        for carbon in c_ind[i]:
            if os.path.isfile(name_hch + '_C%d.gpw'%carbon) == False:
                calc= GPAW( h=0.2,
                            txt = name_hch +'_C%d.txt'%carbon,
                            nbands = 'nao',
                            xc=xc,
                            kpts = kpts,
                            maxiter=1000,
                            #stencils=(3,3),
                            poissonsolver=PoissonSolver(nn=3,eps=1e-12),
                            symmetry={'point_group':False},
                            eigensolver=Davidson(3),
                            occupations=FermiDirac(0.1, fixmagmom=False),
                            spinpol=True,
                            mixer=mixersum,
                            setups={carbon:'hch1s'})

                atoms.set_calculator(calc)

                e_gs = atoms.get_potential_energy() + calc.get_reference_energy()
                e_fermi = calc.get_fermi_level()
                #Harris calc.
                calc.set(txt=name_hch + '_C%d_harris.txt'%carbon,
                         fixdensity = True,
                         kpts=kpts1)
                e_f = calc.get_potential_energy()
                
                calc.write(name_hch + '_C%d.gpw'%carbon)
