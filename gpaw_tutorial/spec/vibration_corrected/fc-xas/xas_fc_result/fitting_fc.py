from lstsq import *
import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import InterpolatedUnivariateSpline
import copy

c_1 = Parameter(0.5)
c_2 = Parameter(0.5)
c_3 = Parameter(0.5)
c_4 = Parameter(0.5)

x_a = 2.82125e2
x_b = 2.85875e2
c_shift = 0.408

names_t = ['ptb_%d'%(i+1) for i in range(3)]+['Ni-C_p4g']
names_e = ['-0.4','0.3','1.2','4.0']

data_t = []
data_e = []
area_t = []
area_e = []

data_ex = np.loadtxt('Fig1_XAS_energy.txt')
mask_e = np.array([x >= x_a and x <= x_b for x in data_ex])

for name_e in names_e:
    data_ey = np.loadtxt('Fig1_XAS_%sps.txt'%name_e)
    data_ey -= data_ey[0]#remove background
    data_e.append([data_ex[mask_e],data_ey[mask_e]])
    area_e.append(np.trapz(data_ey[mask_e],data_ex[mask_e]))

for name_t in names_t:
    data = np.loadtxt('../xas_fc_gen/fc_inp_%s_0/%s_0_XAS_sigma.dat'%(name_t,name_t))
    data_1 = np.loadtxt('../xas_fc_gen/fc_inp_%s_1/%s_1_XAS_sigma.dat'%(name_t,name_t))
    mask_t = np.array([x >= x_a-c_shift and x <= x_b-c_shift for x in data[:,0]])
    data_t.append([data[:,0][mask_t]+c_shift,(data[:,1][mask_t]+data_1[:,1][mask_t])/2.])
    area_t.append(np.trapz((data[:,1][mask_t]+data_1[:,1][mask_t])/2.,data[:,0][mask_t]))

    
fcoef = open('fit_coef_new.dat','w')
fcoef.write('# ptb_1 ptb_2 ptb_3 p4g\n')
for name_ex in names_e:
    data_f = []
    data_p = []
    x_p = np.linspace(x_a,x_b,2048)
    i = names_e.index(name_ex)
    x,y = copy.deepcopy(data_e[i][0]), copy.deepcopy(data_e[i][1])
    for j in range(len(names_t)):
        data_temp = data_t[j][1]/area_t[j]*area_e[i]
        data_f.append(InterpolatedUnivariateSpline(data_t[j][0],data_temp,k=1)(x))
        data_p.append(InterpolatedUnivariateSpline(data_t[j][0],data_temp,k=1)(x_p))
    def f(x): return (np.absolute(c_1())*x[0]+np.absolute(c_2())*x[1]+np.absolute(c_3())*x[2]+np.absolute(c_4())*x[3])/(np.absolute(c_1())+np.absolute(c_2())+np.absolute(c_3())+np.absolute(c_4()))#!

    c = fit(f,[c_1,c_2,c_3,c_4],y,data_f)[0]
    c_sum = np.absolute(c[0])+np.absolute(c[1])+np.absolute(c[2])+np.absolute(c[3])
    fcoef.write('#%sps\n'%name_ex)
    fcoef.write('%.4f       %.4f       %.4f       %.4f\n'%(np.absolute(c[0])/c_sum, np.absolute(c[1])/c_sum, np.absolute(c[2])/c_sum, np.absolute(c[3])/c_sum))
    #-------------R^2 value--------------
    y_bar = np.average(y)
    sse = 0
    sst = 0
    y_fit = (np.absolute(c[0])*data_f[0]+np.absolute(c[1])*data_f[1]+np.absolute(c[2])*data_f[2]+np.absolute(c[3])*data_f[3])/c_sum#!
    for j in range(len(y)):
        sse+=(y[j]-y_fit[j])**2
        sst+=(y[j]-y_bar)**2

    r_sqrd = 1-sse/sst
    #------------------------------------
    x_1 = data_t[0][0]
    plt.figure(1+i)
    plt.plot(x,y,ls='-',label='exp',color='k')
    plt.plot(x_p,(np.absolute(c[0])*data_p[0]+np.absolute(c[1])*data_p[1]+np.absolute(c[2])*data_p[2]+np.absolute(c[3])*data_p[3])/c_sum,ls='-',label='fitted',color='b')#!
    np.savetxt('Fitted_xas_%sps.dat'%name_ex,list(zip(x_p,(np.absolute(c[0])*data_p[0]+np.absolute(c[1])*data_p[1]+np.absolute(c[2])*data_p[2]+np.absolute(c[3])*data_p[3])/c_sum)),fmt = '%.9f')
    #plt.title('%sps, $R^2 = %.3f$'%(name_ex,r_sqrd))
    plt.title('%sps'%(name_ex))
    plt.xlabel('Energy[eV]')
    plt.ylabel('Intensity[a.u.]')
    plt.minorticks_on()
    #plt.ylim(-0.001,0.0185)
    plt.legend(prop={'size':8},loc='upper left')
    plt.savefig('Fitted_%sps.png'%name_ex.replace('.','_'),dpi = 150)
    plt.close()
fcoef.close()
plt.figure(5)
x,y = copy.deepcopy(data_e[0][0]), copy.deepcopy(data_e[0][1])
x1,y1 = copy.deepcopy(data_t[3][0]), copy.deepcopy(data_t[3][1])
plt.plot(x,y,ls='-',label='exp',color='k')
plt.plot(x1,y1/area_t[3]*area_e[0],ls='-',label='theo',color='b')
plt.title('-0.4ps')
plt.xlabel('Energy[eV]')
plt.ylabel('Intensity[a.u.]')
plt.minorticks_on()
plt.legend(prop={'size':8},loc='upper left')
plt.savefig('Fitted_-0_4ps_0.png',dpi = 150)
plt.close()

