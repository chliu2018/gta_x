from lstsq import *
import numpy as np
import matplotlib.pyplot as plt

kw = 'all'
f_l = open('peak_shift_%s.txt'%kw,'w')

mu = Parameter(284.5)
sigma = Parameter(1.0)
height = Parameter(2.e-2)
c = Parameter(7.e-3)

def f(x): return height() * np.exp(-((x-mu())/sigma())**2)+ c()
x = np.loadtxt('Fig1_XAS_energy.txt')
y = np.loadtxt('Fig1_XAS_-0.4ps.txt')
area = np.trapz(y,x)
#mask = np.array([list(x).index(el) > 5 and list(x).index(el) <14 for el in x])
mask = np.array([list(x).index(el) <14 for el in x])
x_c = x[mask]
y_c = y[mask]
coef = fit(f,[mu, sigma, height, c],y_c,x_c)[0]
#coef = fit(f,[mu, sigma, height, c],y,x)[0]

f_l.write('-0.4ps: %.3f\n'%coef[0])

mu1 = Parameter(284.4)
sigma1 = Parameter(0.5)
height1 = Parameter(2.e-2)
c1 = Parameter(7.e-3)

def f1(x): return height1() * np.exp(-((x-mu1())/sigma1())**2)+ c1()

data = np.loadtxt('../xas_fc_gen/fc_inp_Ni-C_p4g_0/Ni-C_p4g_0_XAS_sigma.dat')
data_1 = np.loadtxt('../xas_fc_gen/fc_inp_Ni-C_p4g_1/Ni-C_p4g_1_XAS_sigma.dat')
x1 = data[:,0]
y1 = (data[:,1]+data_1[:,1])/2.
mask1 = np.array([el >= x[0] and el<= x[-1] for el in x1])
x1=x1[mask1]
y1=y1[mask1]
mask2 = np.array([el >= 283.5 and el<= 285.0 for el in x1])
x1_c = x1[mask2]
y1_c = y1[mask2]
area_1 = np.trapz(y1,x1)
con = area/area_1
coef1 = fit(f1,[mu1, sigma1, height1, c1],y1_c,x1_c)[0]

f_l.write('p4g: %.3f\n'%coef1[0])
f_l.write('theory to exp shift: %.3f\n'%(coef[0]-coef1[0]))
f_l.close()

plt.figure(1)
plt.plot(x,y,label='-0.4ps',ls='-',color='r')
plt.plot(x,coef[2] * np.exp(-((x-coef[0])/coef[1])**2)+ coef[3],label='-0.4ps_fit',ls='--',color='r')
plt.plot(x1,y1*con,label='p4g',ls='-',color='b')
plt.plot(x1_c,(coef1[2] * np.exp(-((x1_c-coef1[0])/coef1[1])**2)+ coef1[3])*con,label='p4g_fit',ls='--',color='b')
plt.title('XAS, shift det')
plt.xlabel('Energy(eV)')
plt.ylabel('Intensity(a.u.)')
plt.legend(prop={'size':8},bbox_to_anchor=(0.20, 1.0))
plt.savefig('trial_shift.png',dpi=150)
plt.close()
