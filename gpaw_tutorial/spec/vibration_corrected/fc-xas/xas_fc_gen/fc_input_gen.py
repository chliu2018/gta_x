import numpy as np
from ase.units import *
from ase.io import *
import os

pol = False #if KH code can generate polarized results?

path = '../xas_fc_calc/'
path_pes = '../../pes/'
names = ['ptb_%d'%(i+1) for i in range(3)] + ['Ni-C_p4g']
d_lists = [np.linspace(-0.5,1.5,41)]*4
spins = [0,1]
nibz = 10

def fortran_save(data,filename,fmt):
    np.savetxt(filename,data,fmt = fmt)
    data = np.genfromtxt(filename,dtype=str)
    data = np.core.defchararray.replace(data,'e','D')
    np.savetxt(filename,data,fmt= "%s")

f0 = open('batch_exe.sh','w')
f0.write("sckh=/nfs/home/chliu/sckh/sckh/build/sckh_main\n")
for i in range(len(names)):
    name = names[i]
    d_list = d_lists[i]
    for spin in spins:
        if os.path.isdir('fc_inp_%s_%d'%(name,spin)) == False:
            os.system('mkdir fc_inp_%s_%d'%(name,spin))
        sbdir = 'fc_inp_%s_%d'%(name,spin)
        
        f1 = open('%s/pes_file_list_final.dat'%sbdir,'w')
        f2 = open('%s/dipole_file_list.dat'%sbdir,'w')
        ref_e = [0.0,0.0]#gs,exc

   
        #------------pes_file_initial.dat----------------
        data_gs = np.loadtxt('%sEnergy_%s_pes_gs.txt'%(path_pes,name))
        e_0 = data_gs[np.where(data_gs[:,0] == 0.00)[0][0]][1]
        data_gs[:,1] += ref_e[0]-e_0
        data_gs[:,1] /= Hartree
        mask = np.array([x >= d_list[0] and x <= d_list[-1] for x in data_gs[:,0]])
        fortran_save(data_gs[mask],'%s/pes_file_initial.dat'%sbdir,"%.2f %.10e")
        
        #-------------pes_file_final_n.dat & dipole_file_n.dat--------------
        data_gs = np.loadtxt('%sEnergy_%s_pes_gs.txt'%(path_pes,name))
        data_gs[:,1] += ref_e[0]-e_0
        data_gs = data_gs[mask]
        data_exc = np.loadtxt('%sEnergy_%s_pes_exc.txt'%(path_pes,name))
        data_exc[:,1] += ref_e[1]-e_0
        data_exc = data_exc[mask]
        
        dipole_data = []
        for d in d_list:
            dptmp = np.loadtxt('%s%s_xas_%.2f_C65.%d.dat'%(path,name,d,spin))
            dipole_data.append(dptmp)
        dipole_lens = np.array([len(dipl) for dipl in dipole_data])
        dpl_max = np.amax(dipole_lens)
        dpl_min = np.amin(dipole_lens)
        if dpl_max != dpl_min:
            for p in range(len(dipole_data)):
                dpld = dipole_data[p]
                if len(dpld) != dpl_min:
                    n_exst = len(dpld)/nibz # no. of excited state per kpt at a height
                    #cut from below!
                    mask_d = np.array([(n_exst-k%n_exst-1) < dpl_min/nibz for k in range(len(dpld))])
                    dipole_data[p] = dipole_data[p][mask_d]
        nf = len(dipole_data[0]) #make nstates odd!
        for i in range(nf):
            e_f = []
            d_x = []
            d_y = []
            d_z = []
            x = d_list
            X = np.zeros((len(x),2))
            Y = np.zeros((len(x),4))
            for j in range(len(x)):
                e_corr = (data_exc[j][1]-data_gs[j][1]+dipole_data[j][i][0])/Hartree
                e_f.append(e_corr)
                d_x.append(dipole_data[j][i][1])
                d_y.append(dipole_data[j][i][2])
                if pol == True:
                    d_z.append(dipole_data[j][i][3])
                else:
                    d_z.append(np.zeros_like(dipole_data[j][i][3]))
            X[:,0] = x
            X[:,1] = np.array(e_f)
            fortran_save(X,"%s/pes_file_final_%d.dat"%(sbdir,i),"%.2f %.10e")
            Y[:,0] = x
            Y[:,1] = np.array(d_x)
            Y[:,2] = np.array(d_y)
            Y[:,3] = np.array(d_z)
            fortran_save(Y,"%s/dipole_file_%d.dat"%(sbdir,i),"%.2f %.10e %.10e %.10e")
            f1.write("pes_file_final_%d.dat\n"%i)
            f2.write("dipole_file_%d.dat\n"%i)

            #-------------input.txt-------------------
        f = open('%s/input.txt'%sbdir,'w')
        f.write("""\

runmode XAS

pes_file_i pes_file_initial.dat
pes_file_list_n    pes_file_list_final.dat
dipole_file_list_n dipole_file_list.dat
outfile %s_%d_XAS
npesfile_n %d
vib_solver FOURIER_REAL
npoints_in %d
nstates %d
mu 12.0107
dvr_start_in %.2f
dx_in 0.05
omega_in_start 275
omega_in_end 295
n_omega_in 501
gamma_FWHM 0.3
gamma_inc_FWHM 1.05
gamma_instr_FWHM 0.1
"""%(name,spin,nf,len(d_list),len(d_list),d_list[0]))
        f.close()
        f0.write("""\
cd %s
${sckh} > sckh_log.out 2>&1 &
cd ..
"""%sbdir)
f0.close()
#-----------------------------
