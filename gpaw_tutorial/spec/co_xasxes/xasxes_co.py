from ase.io import *
from gpaw import *

name = 'co'

xc = 'RPBE'
carbon = 1

atoms = read('co_gas_opt.traj@-1')
name_hch = name+'_xas'
name_gs = name+'_gs'
name_xps = name+'_xps'
name_xes = name+'_xes'

#------------------GS-------------------------
calc = GPAW(h = 0.2,
            txt = name_gs +'.txt',
            xc=xc,
            maxiter=1000,
            symmetry={'point_group':False},
            spinpol=False,
)

atoms.set_calculator(calc)
atoms.get_potential_energy()
calc.write(name_gs + '.gpw')
#------------exc-----------------------
calc.set(txt = name_xps +'_C%d.txt'%carbon,
         poissonsolver=PoissonSolver(nn=3,eps=1e-12),
         eigensolver=Davidson(3),
         charge=-1,
         occupations = FermiDirac(0.0,fixmagmom=True),
         spinpol=True,
         setups={carbon:'fch1s'})
calc.get_potential_energy()
calc.write(name_xps + '_C%d.gpw'%carbon)
#--------------------xas---------------
calc.set(nbands='nao',#calculate more unoccupied orbitals
         txt = name_hch +'_C%d.txt'%carbon,
         charge = 0,
         occupations = None,
         spinpol = False,
         setups={carbon:'hch1s'}
)
calc.get_potential_energy()
calc.write(name_hch + '_C%d.gpw'%carbon)
#--------------------xes---------------
calc.set(txt = name_xes +'_C%d.txt'%carbon,
         setups={carbon:'xes1s'}
)
calc.get_potential_energy()
calc.write(name_xes + '_C%d.gpw'%carbon)
