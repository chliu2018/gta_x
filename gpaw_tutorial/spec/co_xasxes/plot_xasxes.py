from ase.io import *
from gpaw import *
from gpaw.xas import *
import numpy as np

name = 'co'
carbon = 1
elem = {1:'C'}
#----------------xas--------------------------
calc=GPAW(name + '_xas_%s%d.gpw'%(elem[carbon],carbon),txt = None)
e_f =calc.get_fermi_level()

calc_gs=GPAW(name + '_gs.gpw',txt = None)
e_gs = calc_gs.get_potential_energy() + calc_gs.get_reference_energy()
    
calc_exc=GPAW(name + '_xps_%s%d.gpw'%(elem[carbon],carbon), txt = None)
e_exc = calc_exc.get_potential_energy() + calc_exc.get_reference_energy()
e_dks=e_exc - e_gs
    
shift_hch = -e_f + e_dks

xas = XAS(calc, mode='xas')
x, y = xas.get_spectra(fwhm=0.5, linbroad=[4.5, -1.0, 5.0])

np.savetxt(name + '_xas_%s%d.dat'%(elem[carbon],carbon), list(zip(x+shift_hch,sum(y),0.5*(y[0,:] + y[1,:]),y[2,:])),fmt='%.9e')
#---------------xes-------------------
shift = False
calc = GPAW(name + '_xes_%s%d.gpw'%(elem[carbon],carbon),txt = None)
calc.initialize()
xes = XAS(calc,mode='xes')
e_f=calc_xes.get_fermi_levels_mean()
x, y = xes.get_spectra(fwhm=0.5)
shift_xes = -e_f
if shift:
    shift_xes += e_dks
np.savetxt('%s_xes_%s%d.dat'%(name,elem[carbon],carbon),zip(x+shift_xes,sum(y), 0.5*(y[0,:] + y[1,:]),y[2,:]),fmt='%.9e') #save data: total, xy, z
