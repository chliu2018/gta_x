#!/bin/bash
#SBATCH --mail-type=ALL
#SBATCH --mail-user=kentmwz@gmail.com
#SBATCH -A SNIC2018-1-5
#SBATCH -J allstruct_xes
#SBATCH -n 96
#SBATCH -t 24:00:00

source $HOME/pfs/bash_files/load_gpaw_201905.bash
mpirun gpaw-python allstruct_xes.py  
