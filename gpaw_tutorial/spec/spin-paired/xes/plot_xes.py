import os
from math import pi, cos, sin
from ase import *
from ase.parallel import rank, barrier
from gpaw import *
from gpaw.atom.generator import *
from gpaw import setup_paths
from gpaw.xas import XAS
#from pylab import *

elem = {16:'C',17:'O',18:'O',19:'O'}
name = 'ts2'
fwhm = 0.5#eV

name_hch = name + '_xas'
name_xes = name + '_xes'
name_xps = name + '_xps'
name_gs = name + '_gs'

shift = False#shift spectra according to DSCF?
if shift:
    dscf_path = '../xas/'

for carbon in [16,17,18,19]:
    calc = GPAW(name_xes + '_%s%d.gpw'%(elem[carbon],carbon), txt=None, idiotproof=False)
    calc.initialize()
    xas = XAS(calc, mode='xes')
    e_f=calc.get_fermi_levels_mean()
    x, y = xas.get_spectra(fwhm=fwhm)
    shift_xes = -e_f
    if shift:
        calc_gs=GPAW(dscf_path + name_gs + '_gs.gpw')
        e_gs = calc_gs.get_potential_energy() + calc_gs.get_reference_energy()
        calc_exc=GPAW(dscf + name_xps + '_xps_%s%d.gpw'%(elem[carbon],carbon))
        e_exc = calc_exc.get_potential_energy() + calc_exc.get_reference_energy()
        e_dks=e_exc - e_gs
        shift_xes += e_dks
    np.savetxt('%s_%s%d.dat'%(name_xes,elem[carbon],carbon),zip(x+shift_xes,sum(y), 0.5*(y[0,:] + y[1,:]),y[2,:]),fmt='%.9e') #save data: total, xy, z
