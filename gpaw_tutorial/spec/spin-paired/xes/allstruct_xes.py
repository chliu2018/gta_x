from ase import *
from ase.calculators.calculator import kptdensity2monkhorstpack as k2m
from gpaw import *
from gpaw import setup_paths
from gpaw.xas import XAS, RecursionMethod
from ase.io import *
import sys

name = 'ts2'
elem = {16:'C',17:'O',18:'O',19:'O'}

name_xes = name + '_xes'

def kpts_2D(atoms,density):
    kpts_size = k2m(atoms,density,even=False)
    kpts_size[2] = 1
    return kpts_size

atoms = read('%s.traj'%name)#for XES we use original small cell
atoms.pbc = 1

kpts = {'size':kpts_2D(atoms,2.5),'gamma':True} 
kpts1 = {'size':kpts_2D(atoms,6.0),'gamma':True}

mixer=Mixer(beta=0.01, nmaxold=5,  weight=100)
mixersum=MixerSum(beta=0.01, nmaxold=5,  weight=100)

#xes calculation gs approx.
for carbon in [16,17,18,19]:
    calc= GPAW( h=0.2,
                kpts = kpts,
                txt = name_xes + '_%s%d.0.txt'%(elem[carbon],carbon),
                xc='RPBE',
                maxiter=1000,
                occupations=FermiDirac(0.1),                
                mixer=mixer,
                setups={carbon:'xes1s'}
    )

    atoms.set_calculator(calc)
    
    e_gs = atoms.get_potential_energy() + calc.get_reference_energy()
    e_fermi = calc.get_fermi_level()

    calc.set(txt=name_xes + '_%s%d.txt'%(elem[carbon],carbon),
             symmetry = {'point_group':False}, #space symmetry
             fixdensity = True,
             kpts = kpts1)
    e_f = calc.get_potential_energy()
    
    calc.write(name_xes + '_%s%d.gpw'%(elem[carbon],carbon))
