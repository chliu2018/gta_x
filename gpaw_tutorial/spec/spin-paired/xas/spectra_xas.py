import numpy as np
import pylab as plt
from cycler import *

name = 'ts2'
#x and y limits
x_1 = {'C':280,'O':520}
x_2 = {'C':320,'O':550}
atom_list = [16,17,18,19]

elem = {16:'C',17:'O',18:'O',19:'O'}
label_list = {16:'C',17:'O(CO)',18:'O(NR)',19:'O(R)'}
pol_list = ['tot','pxy','pz']

for i,carbon in enumerate(atom_list):
    for j,pol in enumerate(pol_list):
        plt.figure(i*len(pol_list)+j+1)

        data = np.loadtxt(name + '_xas_%s%d.dat'%(elem[carbon],carbon))#important!
        x, y=data[:,0], data[:,i+1]
        plt.plot(x, y, label=(name))

        plt.title('XAS_%s_%s%d_%s'%(name,label_list[carbon],carbon,pol))
        plt.xlabel('Energy(eV)')
        plt.ylabel('Intensity(a.u.)')
        plt.legend(prop={'size':8},loc = 'best')
        plt.xlim(x_1[elem[carbon]],x_2[elem[carbon]])
        
        plt.savefig('XAS_%s_%s%d_%s.png'%(name,elem[carbon],carbon,pol, dpi=160)
        plt.close()
