from ase import *
from ase.calculators.calculator import kptdensity2monkhorstpack as k2m
from gpaw import *
from gpaw import setup_paths
from gpaw.xas import XAS, RecursionMethod
from ase.io import *
import numpy as np
import sys

name = 'ts2'
elem = {16:'C',17:'O',18:'O',19:'O'}
recursion = True

def kpts_2D(atoms,density):
    kpts_size = k2m(atoms,density,even=False)
    kpts_size[2] = 1
    return kpts_size

name_hch = name + '_xas'
name_exc = name + '_exc'
name_xps = name + '_xps'
name_gs = name + '_gs'

#change cell to orthorombic 
atoms_0= read('%s.traj'%name)
atoms_0.set_constraint()
atoms = atoms_0*(2,2,1)
cell = atoms.get_cell()
atoms.set_cell([cell[0][0],cell[1][1],cell[2][2]])
atoms.pbc = 1 #for charged system one should use pbc = 1/ (1,1,1)

kpts = {'size':kpts_2D(atoms,2.5),'gamma':True}
kpts1 = {'size':kpts_2D(atoms,6.0),'gamma':True}

mixer=Mixer(beta=0.01, nmaxold=5,  weight=100)
mixersum=MixerSum(beta=0.01, nmaxold=5,  weight=100)

#gs calculation

for carbon in [16,17,18,19]:
    calc= GPAW( h=0.2,
                kpts = kpts,
                txt = name_hch + '_%s%d.txt'%(elem[carbon],carbon),
                xc='RPBE',
                maxiter=1000,
                occupations=FermiDirac(0.1),
                mixer=mixer,
                setups={carbon:'hch1s'}
    )

    atoms.set_calculator(calc)
    
    e_gs = atoms.get_potential_energy() + calc.get_reference_energy()
    e_fermi = calc.get_fermi_level()

    #Harris calc.
    calc.set(txt=name_hch + '_%s%d_harris.txt'%(elem[carbon],carbon),
             symmetry = {'point_group':False}, #space symmetry            
             fixdensity = True,
             kpts=kpts1)
    e_f = calc.get_potential_energy()          
    calc.write(name_hch + '_%s%d.gpw'%(elem[carbon],carbon))

    if recursion:
        #recursion method
        sys.setrecursionlimit(10000)
        calc.set(txt=name_hch + '_%s%d_rec.txt'%(elem[carbon],carbon),kpts = kpts1)
        calc.initialize()
        calc.set_positions()
        
        nvectors = 2000
        r = RecursionMethod(calc)
        
        r.run(nvectors) 
        r.write(name_hch + '_2000_%s%d.rec'%(elem[carbon],carbon),mode='all')
    
