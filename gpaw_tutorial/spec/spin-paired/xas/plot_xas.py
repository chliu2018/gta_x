from ase.io import *
from gpaw import GPAW
from gpaw import setup_paths
from gpaw.xas import XAS, RecursionMethod
import numpy as npy
import sys

elem = {16:'C',17:'O',18:'O',19:'O'}
name = 'ts2'
recursion = True
smear = False

for carbon in [16,17,18,19]:
    sys.setrecursionlimit(10000)

    ##get_fermi_level
    calc=GPAW(name + '_xas_%s%d.gpw'%(elem[carbon],carbon))
    e_f =calc.get_fermi_level()
    #print 'e_f', e_f


    x_start=-10
    x_end= 50
    dx=0.02
    delta=0.3
    fwhm=0.4

    calc_gs=GPAW(name + '_gs.gpw')
    e_gs = calc_gs.get_potential_energy() + calc_gs.get_reference_energy()
    
    calc_exc=GPAW(name + '_xps_%s%d.gpw'%(elem[carbon],carbon))
    e_exc = calc_exc.get_potential_energy() + calc_exc.get_reference_energy()
    e_dks=e_exc - e_gs
    
    shift_hch = -e_f + e_dks
     
    x_rec = x_start + npy.arange(0, x_end - x_start ,dx)

    if recursion:    
        filename = name + '_xas_2000_%s%d.rec'%(elem[carbon],carbon)
        r = RecursionMethod(filename=filename)    
        y = r.get_spectra(x_rec, delta=delta, fwhm=0.4, linbroad=[2.0,0,20], imax=2000)
        
    else:
        xas = XAS(calc, mode='xas')
        x, y = xas.get_spectra(fwhm=0.4, E_in = x_rec,linbroad=[2.0,0,20])

    #postprocess: fd smearing, redistribute intensity
    if smear:
        width=0.1
        fermi = 1./(1 + npy.exp((x_rec-e_f)/width))
        y=y*(1-fermi)
    
    y2 = sum(y)
    
    #save data
    X = npy.zeros((len(x_rec),4))
    X[:,0] = x_rec[:] + shift_hch
    X[:,1] = y2[:]
    X[:,2] = 0.5*(y[0,:]+y[1,:])
    X[:,3] = y[2,:]
    
    npy.savetxt(name + '_xas_%s%d.dat'%(elem[carbon],carbon), X)
