from ase import *
from ase.calculators.calculator import kptdensity2monkhorstpack as k2m
from gpaw import *
from gpaw import setup_paths
from gpaw.xas import XAS, RecursionMethod
from ase.io import *
import numpy as np

name = 'ts2'
elem = {16:'C',17:'O',18:'O',19:'O'}

def kpts_2D(atoms,density):
    kpts_size = k2m(atoms,density,even=False)
    kpts_size[2] = 1
    return kpts_size

name_hch = name + '_xas'
name_exc = name + '_exc'
name_xps = name + '_xps'
name_gs = name + '_gs'

atoms_0 = read('%s.traj'%name)
atoms_0.set_constraint()
atoms = atoms_0*(2,2,1)
atoms.pbc = (1,1,1)#for charged system one should use pbc = 1/ (1,1,1)

kpts = {'size':kpts_2D(atoms,2.5),'gamma':True}

mixer=Mixer(beta=0.01, nmaxold=5,  weight=100)
mixersum=MixerSum(beta=0.01, nmaxold=5,  weight=100)

#gs calculation

for carbon in [16,17,18,19]:
    calc= GPAW( h=0.2,
                kpts = kpts,
                txt = name_xps + '_%s%d.txt'%(elem[carbon],carbon),
                xc='RPBE',
                maxiter=1000,
                charge = -1,
                occupations=FermiDirac(0.1,fixmagmom = True),
                spinpol = True,
                #symmetry = {'point_group':False}, #space symmetry
                mixer=mixersum,
                setups={carbon:'fch1s'}
    )

    atoms.set_calculator(calc)
    
    e_gs = atoms.get_potential_energy() + calc.get_reference_energy()
    e_fermi = calc.get_fermi_level()

    calc.write(name_xps + '_%s%d.gpw'%(elem[carbon],carbon))
