import numpy as np
import matplotlib.pyplot as plt

densities = [2., 2.5, 3.0, 3.2, 3.5, 6.0]
#densities = [ 2.5, 3.0, 3.2]
a = np.linspace(5.0,25.0,2000)
plt.figure(1)
for density in densities:
    N = np.ceil(np.pi*2*density/a)
    plt.plot(a,N,label='density:%.1f$\mathrm{\AA}$'%density)
plt.vlines([5,10,20],0,8.,linestyles='--')
plt.xlabel('lattice param[$\mathrm{\AA}$]')
plt.ylabel('kpts')
plt.legend()
plt.savefig('test_kpts.png',dpi=160)
plt.close()

plt.figure(2)
for density in densities:
    N = np.ceil(np.pi*2*density/a)
    plt.plot(a,a*N,label='density:%.1f$\mathrm{\AA}$'%density)
    print(density, np.ceil(np.pi*2*density),'A')
plt.hlines([15,20,40],5.0,25.,linestyles='--')
plt.xlabel('lattice param[$\mathrm{\AA}$]')
plt.ylabel('aN')
plt.legend(prop={'size':8},loc='best')
plt.savefig('test_kpts_prod.png',dpi=160)
plt.close()

