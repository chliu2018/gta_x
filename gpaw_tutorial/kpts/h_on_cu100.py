from ase.build import fcc100,add_adsorbate
from gpaw import *
from ase import *

name = 'h'
surf = 'cu100'
task = 'scf'
kpts = {'density':6.2,'gamma':True}#kpt density in 1BZ: 2.0 kpts per Å^-1, including Gamma point
xc = 'RPBE'

slab = fcc100('Cu',size = (2,2,4),vacuum = 7.0)
slab.pbc = (1,1,0)
atom = Atom('H')

add_adsorbate(slab,atom,1.25,'hollow')

calc = GPAW(h = 0.2, #grid space 0.2 Å
            kpts = kpts,
            xc = xc,
            symmetry = {'point_group':False}, #space symmetry
            txt = "%s_%s_%s_gpaw.txt"%(name,surf,task),
            mixer = Mixer(0.1, 5, weight=100.0),
            occupations=FermiDirac(0.1)
)

slab.set_calculator(calc)

e_f = slab.get_potential_energy()

f = open('Energy_%s_%s_%s.txt'%(name,surf,task),'w')
f.write('Final energy: %.3f\n'%e_f)
f.close()
