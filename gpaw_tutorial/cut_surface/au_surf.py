from ase import *
from ase.io import * 
from ase.build import bulk,surface,minimize_tilt,niggli_reduce
from ase.geometry import get_duplicate_atoms
import numpy as np

surfs = [(5,3,1),(4,3,2),(3,2,1)]#Miller indices: (531),(432),(321)
multi = [20,17,12]#proportional to, but Not actual number of layers, need trial-and-error
reduc = [[2,2,1],[1,2,1],[1,1,1]]

a = 4.065
au = bulk('Au',a = a, cubic = True)

for i, surf in enumerate(surfs):
    #build surface from bulk
    slab = surface(au, surf, layers = multi[i])
    slab.center(vacuum = 12.5, axis = 2)
    minimize_tilt(slab)
    #niggli reduction
    slab.pbc = 1
    niggli_reduce(slab)
    slab.pbc = (1,1,0)
    #align first cell vector to x-axis
    pos = slab.get_positions()
    cell = slab.get_cell()
    #change Au321 to p cell
    if i == 2:
        cell_a = pos[2] - pos[0]
        cell_b = pos[0]+cell[1]-pos[2]
        cell[0] = cell_a
        cell[1] = cell_b
        slab.set_cell(cell)
        slab.wrap()
    #reduce cell to p(1x1)
    for j in range(3):
        cell[j] /= reduc[i][j]
    slab.set_cell(cell)
    slab.wrap()
    #rotate cell so cell[0] is along x
    v_n = np.cross(cell[0],cell[1])
    v_n /= np.linalg.norm(v_n)
    v_x = np.array([1,0,0])
    angle = np.arccos(np.dot(v_x,cell[0])/(np.linalg.norm(v_x)*np.linalg.norm(cell[0])))/np.pi*180
    if i != 0:
        angle = - angle
    slab.rotate(a = angle, v = v_n,center =  pos[0],rotate_cell = True)
    get_duplicate_atoms(slab,delete = True)
    write('Au_%d%d%d.traj'%(surf[0],surf[1],surf[2]),slab)

    
