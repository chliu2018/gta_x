from ase.build import fcc100,add_adsorbate,molecule
from gpaw import *
from ase import *
from ase.constraints import *
from ase.optimize import *

name = 'co'
surf = 'cu100'
task = 'fs'
kpts = {'density':2.5,'gamma':True}#kpt density in 1BZ: 2.0 kpts per Å^-1, including Gamma point
xc = 'RPBE'

slab = fcc100('Cu',size = (2,2,4),vacuum = 7.0)
slab.pbc = (1,1,0)
molecule = molecule('CO')

add_adsorbate(slab,molecule,1.95,'hollow',mol_index = 1)
#"tag" is the number of layer from top to bottom, if the surface is created with ase.build
mask = [atom.tag > 2 for atom in slab]
c = FixAtoms(mask = mask)
slab.set_constraint(c)
calc = GPAW(h = 0.2, #grid space 0.2 Å
            kpts = kpts,
            xc = xc,
            symmetry = {'point_group':False}, #space symmetry
            txt = "%s_%s_%s_gpaw.txt"%(name,surf,task),
            mixer = Mixer(0.1, 5, weight=100.0),
            occupations=FermiDirac(0.1)
)

slab.set_calculator(calc)

dyn = QuasiNewton(slab,logfile = '%s_%s_%s.log'%(name,surf,task),trajectory = '%s_%s_%s.traj'%(name,surf,task),force_consistent = False)
dyn.run(fmax = 0.05)

e_f = slab.get_potential_energy()

f = open('Energy_%s_%s_%s.txt'%(name,surf,task),'w')
f.write('Final energy: %.3f\n'%e_f)
f.close()
