from ase import *
from ase.io import read, Trajectory
from ase.constraints import FixAtoms
from ase.neb import NEB
from ase.optimize import *
import numpy as np
from ase.parallel import rank, size
from gpaw import *

initial = read('initial.traj')
final = read('final.traj')
xc = 'RPBE'
kpts = {'density':2.5,'gamma':True}

constraint = FixAtoms(mask = [atom.tag > 2 for atom in initial])


nimg = 6# number of images

n = size // nimg     # number of cpu's per image
j = 1 + rank // n  # my image number
assert nimg * n == size

images = [initial]

for i in range(nimg):

    ranks = np.arange(i * n, (i + 1) * n)
    image = initial.copy()

    if rank in ranks:
        mixer=Mixer(0.1, 5, weight=100.0)
        calc = GPAW(h = 0.2,
                    xc=xc,
                    kpts=kpts,
                    txt='neb_%d.txt' % j,
                    communicator=ranks,
                    occupations=FermiDirac(0.1),
                    mixer=mixer,
                    symmetry = {'point_group':False},
                    maxiter = 250
        )
        
        image.set_calculator(calc)
        
    image.set_constraint(constraint)
    images.append(image)
    
images.append(final)

neb = NEB(images, climb=False, parallel=True,method='eb')
#one can use idpp interpolation!
neb.interpolate()

qn=FIRE(neb, dt=0.05, restart='nebfire.hessian',logfile='neb_fire.log')

traj = Trajectory('neb_%d.traj' % j, 'w', images[j], master=(rank % n == 0))

qn.attach(traj)
qn.run(fmax=0.1)
