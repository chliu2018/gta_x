from ase import *
from ase.io import read, Trajectory
from ase.constraints import FixAtoms
from ase.neb import NEB
from ase.optimize import *
import numpy as np
from ase.parallel import rank, size
from gpaw import *

initial = read('initial.traj')
final = read('final.traj')
xc = 'RPBE'
kpts = {'density':2.5,'gamma':True}

nimg = 6

n = size // nimg     # number of cpu's per image
j = 1 + rank // n  # my image number
assert nimg * n == size

images = [initial]
images += read('neb_geo.traj@1:%d'%(nimg+1))
images += [final]

for i in range(nimg):

    ranks = np.arange(i * n, (i + 1) * n)
    #image = initial.copy()
    image = images[i+1]

    if rank in ranks:
        mixer=Mixer(0.1, 5, weight=100.0)
        calc = GPAW(h=0.2,
                    xc=xc,
                    kpts=kpts,#TAKE CARE OF SLAB SIZE!
                    txt='neb_%d.2.txt' % j,
                    communicator=ranks,
                    occupations=FermiDirac(0.1),
                    symmetry = {'point_group':False},
                    mixer=mixer,
                    maxiter = 250
                    )
        
        image.set_calculator(calc)

    constraint = FixAtoms(mask = [atom.tag > 2 for atom in image])
    image.set_constraint(constraint)
    
neb = NEB(images, climb=True, parallel=True,method='eb')

qn=FIRE(neb, dt=0.05, restart='nebfire.hessian',logfile='neb_fire.log')

traj = Trajectory('neb_%d.2.traj' % j, 'w', images[j],master=(rank % n == 0))

qn.attach(traj)
qn.run(fmax=0.1)
