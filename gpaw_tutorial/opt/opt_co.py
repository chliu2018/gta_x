from ase.build import molecule
from gpaw import *
from ase import *
from ase.optimize import *

name = 'co'
surf = 'gas'
task = 'opt'
proj = '{}_{}_{}'.format(name,surf,task)
xc = 'RPBE'

atoms = molecule('CO',cell=[12.]*3)
atoms.center()
atoms.pbc = 0

calc = GPAW(h = 0.2, #grid space 0.2 A
            xc = xc,
            #symmetry = {'point_group':False}, #space symmetry
            txt = "{}_gpaw.txt".format(proj),
)

atoms.set_calculator(calc)

dyn = QuasiNewton(atoms,logfile = '{}.log'.format(proj),trajectory = '{}.traj'.format(proj),force_consistent = False)
dyn.run(fmax = 0.05)

e_f = atoms.get_potential_energy()

f = open('Energy_{}.txt'.format(proj),'w')
f.write('Final energy: %.3f\n'%e_f)
f.close()
